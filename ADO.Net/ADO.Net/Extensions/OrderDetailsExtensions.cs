﻿using System.Collections.Generic;
using System.Text;
using ADO.Net.Models;

namespace ADO.Net.Extensions
{
    public static class OrderDetailsExtensions
    {
        public static string BuildQuery(this IEnumerable<OrderDetail> orderDetails, int orderID)
        {
            const string space = " ";

            const string sqlPattern = "INSERT dbo.[Order Details] (" +
                " [OrderID]," +
                " [ProductID]," +
                " [UnitPrice]," +
                " [Quantity]," +
                " [Discount]) " +
                " VALUES({0}, {1}, {2}, {3}, {4});";

            if (orderDetails == null)
            {
                return string.Empty;
            }

            var query = new StringBuilder();

            foreach (var orderDetail in orderDetails)
            {
                var subQuery = string.Format(sqlPattern,
                    orderID.ToString(),
                    orderDetail.ProductID.ToString(),
                    orderDetail.UnitPrice.ToString().Replace(",", "."),
                    orderDetail.Quantity.ToString(),
                    orderDetail.Discount.ToString().Replace(",", ".")) + space;

                query.Append(subQuery);
            }

            return query.ToString().Trim();
        }
    }
}
