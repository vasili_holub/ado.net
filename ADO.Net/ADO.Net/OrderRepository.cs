﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using ADO.Net.Emun;
using ADO.Net.Extensions;
using ADO.Net.Models;

namespace ADO.Net
{
    public class OrderRepository : IOrderRepository
    {
        private readonly DbProviderFactory _providerFactory;

        private readonly string _connectionString;

        public OrderRepository(string connectionString, DbProviderFactory providerFactory)
        {
            _connectionString = connectionString;
            _providerFactory = providerFactory;
        }

        public Order CreateOrder(Order order)
        {
            if (order == null)
            {
                return null;
            }

            if (OrderInfoExists("Orders", order.OrderID))
            {
                throw new ArgumentException(nameof(CreateOrder));
            }

            if (OrderInfoExists("Order Details", order.OrderID))
            {
                throw new ArgumentException(nameof(CreateOrder));
            }

            var newOrderId = 0;
            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "INSERT dbo.Orders([CustomerID], [EmployeeID], [RequiredDate], [ShipVia], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) " +
                                          "output INSERTED.OrderID " +
                                          "VALUES           (@CustomerID,  @EmployeeID,  @RequiredDate,  @ShipVia,  @Freight,  @ShipName,  @ShipAddress,  @ShipCity,  @ShipRegion,  @ShipPostalCode,  @ShipCountry); ";
                        
                    command.CommandType = CommandType.Text;

                    // Adding parameters for preventing sql injection.
                    var paramCustomerID = command.CreateParameter(); paramCustomerID.ParameterName = "CustomerID"; paramCustomerID.Value = order.CustomerID ?? string.Empty; command.Parameters.Add(paramCustomerID);
                    var paramEmployeeID = command.CreateParameter(); paramEmployeeID.ParameterName = "EmployeeID"; paramEmployeeID.Value = order.EmployeeID ?? 0; command.Parameters.Add(paramEmployeeID);
                    var paramRequiredDate = command.CreateParameter(); paramRequiredDate.ParameterName = "RequiredDate"; paramRequiredDate.Value = order.RequiredDate; command.Parameters.Add(paramRequiredDate);
                    var paramShipVia = command.CreateParameter(); paramShipVia.ParameterName = "ShipVia"; paramShipVia.Value = order.ShipVia ?? 0; command.Parameters.Add(paramShipVia);
                    var paramFreight = command.CreateParameter(); paramFreight.ParameterName = "Freight"; paramFreight.Value = order.Freight ?? 0m; command.Parameters.Add(paramFreight);
                    var paramShipName = command.CreateParameter(); paramShipName.ParameterName = "ShipName"; paramShipName.Value = order.ShipName ?? SqlString.Null; command.Parameters.Add(paramShipName);
                    var paramShipAddress = command.CreateParameter(); paramShipAddress.ParameterName = "ShipAddress"; paramShipAddress.Value = order.ShipAddress ?? SqlString.Null; command.Parameters.Add(paramShipAddress);
                    var paramShipCity = command.CreateParameter(); paramShipCity.ParameterName = "ShipCity"; paramShipCity.Value = order.ShipCity ?? SqlString.Null; command.Parameters.Add(paramShipCity);
                    var paramShipRegion = command.CreateParameter(); paramShipRegion.ParameterName = "ShipRegion"; paramShipRegion.Value = order.ShipRegion ?? SqlString.Null; command.Parameters.Add(paramShipRegion);
                    var paramShipPostalCode = command.CreateParameter(); paramShipPostalCode.ParameterName = "ShipPostalCode"; paramShipPostalCode.Value = order.ShipPostalCode ?? SqlString.Null; command.Parameters.Add(paramShipPostalCode);
                    var paramShipCountry = command.CreateParameter(); paramShipCountry.ParameterName = "ShipCountry"; paramShipCountry.Value = order.ShipCountry ?? SqlString.Null; command.Parameters.Add(paramShipCountry);

                    // We devided inserting process into two parts: to insert main order data and get id.
                    try
                    {
                        newOrderId = (int)command.ExecuteScalar();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }   

                    // And second part: to insert nested classes with recently got order id.
                    command.CommandText = order.OrderDetails.BuildQuery(newOrderId);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }

                }
            }

            order.OrderID = newOrderId;
            List<OrderDetail> orderDetails = new List<OrderDetail>(order.OrderDetails);
            for (int i = 0; i < orderDetails.Count; i++)
            {
                orderDetails[i].OrderID = newOrderId;
            }
            order.OrderDetails = orderDetails;

            return order;
        }

        public Order DeleteOrder(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(CreateOrder));
            }

            var currentOrder = GetOrderByID(order.OrderID);
            if (GetOrderByID(order.OrderID) == null)
            {
                return null;
            }

            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM dbo.[Order Details] WHERE OrderID = @orderID; " +
                        "DELETE FROM dbo.Orders WHERE OrderID = @orderID;";

                    command.CommandType = CommandType.Text;

                    var paramId = command.CreateParameter();
                    paramId.ParameterName = "orderID";
                    paramId.Value = order.OrderID;
                    command.Parameters.Add(paramId);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
            }

            return currentOrder;
        }

        public Order GetOrderByID(int id)
        {
            var order = new Order();

            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT OrderID, CustomerID, EmployeeID, OrderDate, RequiredDate, " +
                        "ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, " +
                        "ShipPostalCode, ShipCountry FROM dbo.Orders WHERE OrderID = @orderID; " +
                        "SELECT o.OrderID, o.ProductID, o.UnitPrice, o.Quantity, o.Discount, p.ProductName" +
                        " FROM dbo.[Order Details] as o INNER JOIN dbo.Products as p" +
                        " ON o.ProductID = p.ProductID" +
                        " WHERE o.OrderID = @orderID;";

                    command.CommandType = CommandType.Text;
                    
                    var paramId = command.CreateParameter();
                    paramId.ParameterName = "orderID";
                    paramId.Value = id;

                    command.Parameters.Add(paramId);

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            return null;
                        }

                        while (reader.Read())
                        {
                            order.OrderID = reader.GetInt32("OrderID");
                            order.CustomerID = reader.IsDBNull("CustomerID") ? null : reader.GetString("CustomerID");
                            order.EmployeeID = reader.IsDBNull("EmployeeID") ? null : (int?)reader.GetInt32("EmployeeID");
                            order.OrderDate = reader.IsDBNull("OrderDate") ? null : (DateTime?)reader.GetDateTime("OrderDate");
                            order.RequiredDate = reader.IsDBNull("RequiredDate") ? null : (DateTime?)reader.GetDateTime("RequiredDate");
                            order.ShippedDate = reader.IsDBNull("ShippedDate") ? null : (DateTime?)reader.GetDateTime("ShippedDate");
                            order.ShipVia = reader.IsDBNull("ShipVia") ? null : (int?)reader.GetInt32("ShipVia");
                            order.Freight = reader.IsDBNull("Freight") ? null : (decimal?)reader.GetDecimal("Freight");
                            order.ShipName = reader.IsDBNull("ShipName") ? null : reader.GetString("ShipName");
                            order.ShipAddress = reader.IsDBNull("ShipAddress") ? null : reader.GetString("ShipAddress");
                            order.ShipCity = reader.IsDBNull("ShipCity") ? null : reader.GetString("ShipCity");
                            order.ShipRegion = reader.IsDBNull("ShipRegion") ? null : reader.GetString("ShipRegion");
                            order.ShipPostalCode = reader.IsDBNull("ShipPostalCode") ? null : reader.GetString("ShipPostalCode");
                            order.ShipCountry = reader.IsDBNull("ShipCountry") ? null : reader.GetString("ShipCountry");
                            order.OrderStatus = GetOrderStatus(order.OrderDate, order.ShippedDate);

                            reader.NextResult();

                            var orderDetails = new List<OrderDetail>();

                            while (reader.Read())
                            {
                                var orderDetail = new OrderDetail
                                {
                                    OrderID = reader.GetInt32("OrderID"),
                                    ProductID = reader.GetInt32("ProductID"),
                                    UnitPrice = reader.GetDecimal("UnitPrice"),
                                    Quantity = reader.GetInt16("Quantity"),
                                    Discount = reader.GetFloat("Discount"),
                                    ProductName = reader.GetString("ProductName")
                                };

                                orderDetails.Add(orderDetail);
                            }

                            order.OrderDetails = orderDetails;
                        }
                    }
                }

                return order;
            }
        }

        public IEnumerable<Order> GetOrders()
        {
            var resultOrders = new List<Order>();

            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT OrderID, CustomerID, EmployeeID, OrderDate, RequiredDate, " +
                        "ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, " +
                        "ShipPostalCode, ShipCountry FROM dbo.Orders";
                    command.CommandType = CommandType.Text;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var order = new Order();
                            order.OrderID = reader.GetInt32("OrderID");
                            order.CustomerID = reader.IsDBNull("CustomerID") ? null : reader.GetString("CustomerID");
                            order.EmployeeID = reader.IsDBNull("EmployeeID") ? null : (int?)reader.GetInt32("EmployeeID");
                            order.OrderDate = reader.IsDBNull("OrderDate") ? null : (DateTime?)reader.GetDateTime("OrderDate");
                            order.RequiredDate = reader.IsDBNull("RequiredDate") ? null : (DateTime?)reader.GetDateTime("RequiredDate");
                            order.ShippedDate = reader.IsDBNull("ShippedDate") ? null : (DateTime?)reader.GetDateTime("ShippedDate");
                            order.ShipVia = reader.IsDBNull("ShipVia") ? null : (int?)reader.GetInt32("ShipVia");
                            order.Freight = reader.IsDBNull("Freight") ? null : (decimal?)reader.GetDecimal("Freight");
                            order.ShipName = reader.IsDBNull("ShipName") ? null : reader.GetString("ShipName");
                            order.ShipAddress = reader.IsDBNull("ShipAddress") ? null : reader.GetString("ShipAddress");
                            order.ShipCity = reader.IsDBNull("ShipCity") ? null : reader.GetString("ShipCity");
                            order.ShipRegion = reader.IsDBNull("ShipRegion") ? null : reader.GetString("ShipRegion");
                            order.ShipPostalCode = reader.IsDBNull("ShipPostalCode") ? null : reader.GetString("ShipPostalCode");
                            order.ShipCountry = reader.IsDBNull("ShipCountry") ? null : reader.GetString("ShipCountry");
                            resultOrders.Add(order);
                        }
                    }
                }
            }

            return resultOrders;
        }

        public Order UpdateOrder(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(CreateOrder));
            }

            var currentOrder = GetOrderByID(order.OrderID);

            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "UPDATE dbo.Orders" +
                        " SET CustomerID = @CustomerID," +
                        " EmployeeID = @EmployeeID," +
                        " RequiredDate = @RequiredDate," +
                        " ShipVia = @ShipVia," +
                        " Freight = @Freight," +
                        " ShipName = @ShipName," +
                        " ShipAddress = @ShipAddress," +
                        " ShipCity = @ShipCity," +
                        " ShipRegion = @ShipRegion," +
                        " ShipPostalCode = @ShipPostalCode," +
                        " ShipCountry = @ShipCountry" +
                        " WHERE OrderID = @OrderID;" +
                        " DELETE FROM dbo.[Order Details]" + // Deleting old order details.
                        " WHERE OrderID = @OrderID;" +
                        order.OrderDetails.BuildQuery(order.OrderID);

                    command.CommandType = CommandType.Text;

                    // Adding parameters for preventing sql injection.
                    var paramOrderID = command.CreateParameter(); paramOrderID.ParameterName = "OrderID"; paramOrderID.Value = order.OrderID; command.Parameters.Add(paramOrderID);
                    var paramCustomerID = command.CreateParameter(); paramCustomerID.ParameterName = "CustomerID"; paramCustomerID.Value = order.CustomerID ?? SqlString.Null; command.Parameters.Add(paramCustomerID);
                    var paramEmployeeID = command.CreateParameter(); paramEmployeeID.ParameterName = "EmployeeID"; paramEmployeeID.Value = order.EmployeeID ?? 0; command.Parameters.Add(paramEmployeeID);
                    var paramRequiredDate = command.CreateParameter(); paramRequiredDate.ParameterName = "RequiredDate"; paramRequiredDate.Value = order.RequiredDate; command.Parameters.Add(paramRequiredDate);
                    var paramShipVia = command.CreateParameter(); paramShipVia.ParameterName = "ShipVia"; paramShipVia.Value = order.ShipVia ?? 0; command.Parameters.Add(paramShipVia);
                    var paramFreight = command.CreateParameter(); paramFreight.ParameterName = "Freight"; paramFreight.Value = order.Freight ?? 0; command.Parameters.Add(paramFreight);
                    var paramShipName = command.CreateParameter(); paramShipName.ParameterName = "ShipName"; paramShipName.Value = order.ShipName ?? SqlString.Null; command.Parameters.Add(paramShipName);
                    var paramShipAddress = command.CreateParameter(); paramShipAddress.ParameterName = "ShipAddress"; paramShipAddress.Value = order.ShipAddress ?? SqlString.Null; command.Parameters.Add(paramShipAddress);
                    var paramShipCity = command.CreateParameter(); paramShipCity.ParameterName = "ShipCity"; paramShipCity.Value = order.ShipCity ?? SqlString.Null; command.Parameters.Add(paramShipCity);
                    var paramShipRegion = command.CreateParameter(); paramShipRegion.ParameterName = "ShipRegion"; paramShipRegion.Value = order.ShipRegion ?? SqlString.Null; command.Parameters.Add(paramShipRegion);
                    var paramShipPostalCode = command.CreateParameter(); paramShipPostalCode.ParameterName = "ShipPostalCode"; paramShipPostalCode.Value = order.ShipPostalCode ?? SqlString.Null; command.Parameters.Add(paramShipPostalCode);
                    var paramShipCountry = command.CreateParameter(); paramShipCountry.ParameterName = "ShipCountry"; paramShipCountry.Value = order.ShipCountry ?? SqlString.Null; command.Parameters.Add(paramShipCountry);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }

                    order.OrderDate = currentOrder.OrderDate;
                    order.ShippedDate = currentOrder.ShippedDate;

                    return order;
                }
            }
        }

        public void MarkOrderAsDone(DateTime shippedDate, int orderID)
        {
            SetDateInOrder("ShippedDate", shippedDate, orderID);
        }

        public void MarkOrderAsPassedToWork(DateTime orderDate, int orderID)
        {
            SetDateInOrder("OrderDate", orderDate, orderID);
        }

        public IEnumerable<GoodItemBought> GetGoodItemsBought(string customerID)
        {
            List<GoodItemBought> goodItemsBought = new List<GoodItemBought>();

            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "CustOrderHist";
                    command.CommandType = CommandType.StoredProcedure;

                    var paramCustomID = command.CreateParameter();
                    paramCustomID.ParameterName = "CustomerID";
                    paramCustomID.Value = customerID;
                    command.Parameters.Add(paramCustomID);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var goodItemBought = new GoodItemBought
                            {
                                ProductName = reader.IsDBNull("ProductName") ? null : reader.GetString("ProductName"),
                                Total = reader.GetInt32("Total")
                            };

                            goodItemsBought.Add(goodItemBought);
                        }
                    }
                }
            }
            return goodItemsBought;
        }

        public IEnumerable<ExtendedOrderDetail> GetExtendedOrderDetails(int orderID)
        {
            List<ExtendedOrderDetail> extendedOrderDetails = new List<ExtendedOrderDetail>();

            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "CustOrdersDetail";
                    command.CommandType = CommandType.StoredProcedure;

                    var paramOrderID = command.CreateParameter();
                    paramOrderID.ParameterName = "OrderID";
                    paramOrderID.Value = orderID;
                    command.Parameters.Add(paramOrderID);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var extendedOrderDetail = new ExtendedOrderDetail
                            {
                                ProductName = reader.IsDBNull("ProductName") ? null : reader.GetString("ProductName"),
                                Discount = reader.GetInt32("Discount"),
                                ExtendedPrice = reader.GetDecimal("ExtendedPrice"),
                                Quantity = reader.GetInt16("Quantity"),
                                UnitPrice = reader.GetDecimal("UnitPrice")
                            };

                            extendedOrderDetails.Add(extendedOrderDetail);
                        }
                    }
                }
            }
            return extendedOrderDetails;
        }

        private void SetDateInOrder(string fieldName, DateTime date, int orderID)
        {
            if (!OrderInfoExists("Orders", orderID))
            {
                throw new Exception(nameof(SetDateInOrder));
            }

            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = $"UPDATE dbo.Orders SET {fieldName} = @date" +
                        " WHERE OrderID = @orderID";
                    command.CommandType = CommandType.Text;

                    var paramDate = command.CreateParameter();
                    paramDate.ParameterName = "date";
                    paramDate.Value = date;
                    command.Parameters.Add(paramDate);

                    var paramOrderID = command.CreateParameter();
                    paramOrderID.ParameterName = "orderID";
                    paramOrderID.Value = orderID;
                    command.Parameters.Add(paramOrderID);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
            }
        }

        private OrderStatus GetOrderStatus(DateTime? orderDate, DateTime? shippedDate)
        {
            if (shippedDate != null)
            {
                return OrderStatus.Done;
            }

            if (orderDate != null)
            {
                return OrderStatus.InProcess;
            }

            return OrderStatus.New;
        }

        private bool OrderInfoExists(string tableName, int orderID)
        {
            using (var connection = _providerFactory.CreateConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = $"SELECT * FROM dbo.[{tableName}] WHERE OrderID = @orderID;";
                    command.CommandType = CommandType.Text;

                    var paramOrderID = command.CreateParameter();
                    paramOrderID.ParameterName = "orderID";
                    paramOrderID.Value = orderID;

                    command.Parameters.Add(paramOrderID);

                    try
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            return reader.HasRows;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
            }
        }
    }
}
