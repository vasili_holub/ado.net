﻿namespace ADO.Net.Emun
{
    public enum OrderStatus
    {
        New,
        InProcess,
        Done
    }
}
