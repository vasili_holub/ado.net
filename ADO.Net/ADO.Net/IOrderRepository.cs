﻿using System;
using System.Collections.Generic;
using ADO.Net.Models;

namespace ADO.Net
{
    /// <summary>
    /// Interface of orders' repository.
    /// </summary>
    public interface IOrderRepository
    {
        /// <summary>
        /// Retrieves all orders.
        /// </summary>
        /// <returns>Orders' list.</returns>
        IEnumerable<Order> GetOrders();

        /// <summary>
        /// Retrieves an order by id.
        /// </summary>
        /// <param name="id">Order identifier.</param>
        /// <returns>Order.</returns>
        Order GetOrderByID(int id);

        /// <summary>
        /// Adds an order to the repository.
        /// </summary>
        /// <param name="order">Order which to be added.</param>
        /// <returns>Order which have been added  with assigned id.</returns>
        Order CreateOrder(Order order);

        /// <summary>
        /// Updates an order.
        /// </summary>
        /// <param name="order">Order with updated elements.</param>
        /// <returns>Updated an order in the repository.</returns>
        Order UpdateOrder(Order order);

        /// <summary>
        /// Deletes an order.
        /// </summary>
        /// <param name="order">Order which to be deleted.</param>
        /// <returns>Order which just been deleted.</returns>
        Order DeleteOrder(Order order);

        /// <summary>
        /// Set order date to some specific.
        /// </summary>
        /// <param name="orderDate"></param>
        void MarkOrderAsPassedToWork(DateTime orderDate, int orderID);

        /// <summary>
        /// Set shipped date to some specific.
        /// </summary>
        /// <param name="shippedDate"></param>
        void MarkOrderAsDone(DateTime shippedDate, int orderID);

        /// <summary>
        /// Retrieves information about goods' quantities bought by some customer.
        /// </summary>
        /// <param name="customerID">Customer identifier.</param>
        /// <returns>List of information about goods' 
        /// quantities bought by some customer.</returns>
        IEnumerable<GoodItemBought> GetGoodItemsBought(string customerID);

        /// <summary>
        /// Retrieves order details.
        /// </summary>
        /// <param name="customerID">Order identifier.</param>
        /// <returns>Order details.</returns>
        IEnumerable<ExtendedOrderDetail> GetExtendedOrderDetails(int orderID);
    }
}
