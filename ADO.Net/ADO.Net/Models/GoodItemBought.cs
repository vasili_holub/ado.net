﻿namespace ADO.Net.Models
{
    public class GoodItemBought
    {
        public string ProductName { get; set; }

        public int Total { get; set; }
    }
}
