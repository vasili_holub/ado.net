﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using ADO.Net.Emun;
using ADO.Net.Models;
using FluentAssertions;
using Xunit;

namespace ADO.Net.UnitTests
{
    [TestCaseOrderer(PriorityOrderer.TypeName, PriorityOrderer.AssembyName)]
    public  class OrderRepositoryTests
    {
        private const string _connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private const string _providerInvariantName = "System.Data.SqlClient";

        private const int _expectedOrdersCount = 831;

        private readonly DbProviderFactory _dbProviderFactory;

        private static Order _lastAddedOrder;

        private Order ExpectedOrder
        {
            get
            {
                var order = new Order
                {
                    OrderID = 10255,
                    CustomerID = "RICSU",
                    EmployeeID = 9,
                    OrderDate = new DateTime(1996, 7, 12),
                    RequiredDate = new DateTime(1996, 8, 9),
                    ShippedDate = new DateTime(1996, 7, 15),
                    ShipVia = 3,
                    Freight = 148.33m,
                    ShipName = "Richter Supermarkt",
                    ShipAddress = "Starenweg 5",
                    ShipCity = "Genève",
                    ShipRegion = null,
                    ShipPostalCode = "1204",
                    ShipCountry = "Switzerland",
                    OrderStatus = OrderStatus.Done,

                    OrderDetails = new List<OrderDetail>()
                    {
                        new OrderDetail() { OrderID = 10255, ProductID = 2, UnitPrice = 15.20m, Quantity = 20, Discount = 0, ProductName = "Chang" },
                        new OrderDetail() { OrderID = 10255, ProductID = 16, UnitPrice = 13.90m, Quantity = 35, Discount = 0, ProductName = "Pavlova" },
                        new OrderDetail() { OrderID = 10255, ProductID = 36, UnitPrice = 15.20m, Quantity = 25, Discount = 0, ProductName = "Inlagd Sill" },
                        new OrderDetail() { OrderID = 10255, ProductID = 59, UnitPrice = 44.00m, Quantity = 30, Discount = 0, ProductName = "Raclette Courdavault" }
                    }
                };

                return order;
            }
        }

        private Order NewOrder
        {
            get
            {
                var order = new Order
                {
                    OrderID = 19999,
                    CustomerID = "RICSU",
                    EmployeeID = 9,
                    OrderDate = new DateTime(1996, 7, 12),
                    RequiredDate = new DateTime(1996, 8, 9),
                    ShippedDate = new DateTime(1996, 7, 15),
                    ShipVia = 3,
                    Freight = 148.33m,
                    ShipName = "Richter Supermarkt",
                    ShipAddress = "Starenweg 5",
                    ShipCity = "Genève",
                    ShipRegion = null,
                    ShipPostalCode = "1204",
                    ShipCountry = "Switzerland",
                    OrderStatus = OrderStatus.New,

                    OrderDetails = new List<OrderDetail>()
                    {
                        new OrderDetail() { OrderID = 19999, ProductID = 2, UnitPrice = 15.20m, Quantity = 20, Discount = 0, ProductName = "Chang" },
                        new OrderDetail() { OrderID = 19999, ProductID = 16, UnitPrice = 13.90m, Quantity = 35, Discount = 0, ProductName = "Pavlova" },
                        new OrderDetail() { OrderID = 19999, ProductID = 36, UnitPrice = 15.20m, Quantity = 25, Discount = 0, ProductName = "Inlagd Sill" },
                        new OrderDetail() { OrderID = 19999, ProductID = 59, UnitPrice = 44.00m, Quantity = 30, Discount = 0, ProductName = "Raclette Courdavault" }
                    }
                };

                return order;
            }
        }

        private List<GoodItemBought> GoodItemsBought => new List<GoodItemBought>()
        {
            new GoodItemBought() {ProductName = "Camembert Pierrot", Total = 10},
            new GoodItemBought() {ProductName = "Gudbrandsdalsost", Total = 1},
            new GoodItemBought() {ProductName = "Konbu", Total = 10},
            new GoodItemBought() {ProductName = "Mascarpone Fabioli", Total = 10},
            new GoodItemBought() {ProductName = "Mozzarella di Giovanni", Total = 10},
            new GoodItemBought() {ProductName = "Outback Lager", Total = 5},
            new GoodItemBought() {ProductName = "Queso Cabrales", Total = 2},
            new GoodItemBought() {ProductName = "Singaporean Hokkien Fried Mee", Total = 5},
            new GoodItemBought() {ProductName = "Teatime Chocolate Biscuits", Total = 7},
            new GoodItemBought() {ProductName = "Tofu", Total = 3}
        };

        private string TestCustomerID => "ANATR";

        private List<ExtendedOrderDetail> ExtendedOrderDetails => new List<ExtendedOrderDetail>()
        {
            new ExtendedOrderDetail() {ProductName = "Chang", UnitPrice = 15.20m, Quantity = 50, Discount = 20, ExtendedPrice = 608.00m},
            new ExtendedOrderDetail() {ProductName = "Chef Anton's Gumbo Mix", UnitPrice = 17.00m, Quantity = 65, Discount = 20, ExtendedPrice = 884.00m},
            new ExtendedOrderDetail() {ProductName = "Mascarpone Fabioli", UnitPrice = 25.60m, Quantity = 6, Discount = 20, ExtendedPrice = 122.88m}
        };

        private int TestOrderID => 10258;

        public OrderRepositoryTests()
        {
            _dbProviderFactory = SqlClientFactory.Instance;
        }

        [Fact, TestPriority(1)]
        public void GetOrders_Success_Match_Count()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));

            // Act
            var orders = orderRepository.GetOrders();
            var actual = orders.Count();

            // Assert
            Assert.Equal(_expectedOrdersCount, actual);
        }

        [Fact, TestPriority(2)]
        public void GetOrderById_Success_Match_Orders()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));
            var expectedOrder = ExpectedOrder;

            // Act
            var actualOrder = orderRepository.GetOrderByID(expectedOrder.OrderID);

            // Assert
            expectedOrder.Should().BeEquivalentTo(actualOrder, options => options.IncludingNestedObjects());
        }

        [Fact, TestPriority(3)]
        public void GetOrderById_Not_Success_Match_Orders()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));
            var expectedOrder = ExpectedOrder;
            expectedOrder.ShipVia = 2; // To change from right to the wrong value intentionally.

            // Act
            var actualOrder = orderRepository.GetOrderByID(expectedOrder.OrderID);

            // Assert
            expectedOrder.Should().NotBeEquivalentTo(actualOrder, options => options.IncludingNestedObjects());
        }

        [Fact, TestPriority(4)]
        public void CreateOrder_Success()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));
            var expected = NewOrder;
            // We set order date and shipped date to null as far as according to task conditions, 
            // we can't change values of this properties directly.
            expected.OrderDate = null;
            expected.ShippedDate = null;

            // Act
            var createdOrder = orderRepository.CreateOrder(expected);

            // Assert
            var newlyAddedOrderId = createdOrder.OrderID;

            // We add expected order with newly added order id, because we don't know it at the test running moment.
            expected.OrderID = newlyAddedOrderId;

            List<OrderDetail> orderDetails = new List<OrderDetail>(expected.OrderDetails);
            for (int i = 0; i < orderDetails.Count; i++)
            {
                orderDetails[i].OrderID = newlyAddedOrderId;
            }
            expected.OrderDetails = orderDetails;

            // Get added order by its id.
            var actual = orderRepository.GetOrderByID(newlyAddedOrderId);

            // Setting static field to insure accomplishing test case on deletion.
            _lastAddedOrder = actual;

            expected.Should().BeEquivalentTo(actual, options => options.IncludingNestedObjects());
        }

        [Fact, TestPriority(5)]
        public void UpdateOrder_Success()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));
            var expected = _lastAddedOrder;
            expected.ShipName = "New Ship Name";
            expected.ShipVia = 2;
            expected.Freight = 148.35m;
            expected.ShipAddress = "Starenweg 6";
            expected.ShipCity = "Moscow";
            expected.ShipRegion = "Some Region";
            expected.ShipCountry = "Russia";
            expected.RequiredDate = new DateTime(2020, 1, 1);

            // Act
            orderRepository.UpdateOrder(expected);

            // Assert
            var actual = orderRepository.GetOrderByID(expected.OrderID);
            expected.Should().BeEquivalentTo(actual, options => options.IncludingNestedObjects());
        }

        [Fact, TestPriority(6)]
        public void MarkOrderAsPassedToWork_Success()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));
            var expected = _lastAddedOrder;
            expected.OrderDate = new DateTime(2020, 1, 1);
            expected.OrderStatus = OrderStatus.InProcess;

            // Act
            orderRepository.MarkOrderAsPassedToWork(new DateTime(2020, 1, 1), _lastAddedOrder.OrderID);

            // Assert
            var actual = orderRepository.GetOrderByID(_lastAddedOrder.OrderID);
            expected.Should().BeEquivalentTo(actual, options => options.IncludingNestedObjects());
        }

        [Fact, TestPriority(7)]
        public void MarkOrderAsDone_Success()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));
            var expected = _lastAddedOrder;
            expected.ShippedDate = new DateTime(2020, 1, 1);
            expected.OrderStatus = OrderStatus.Done;

            // Act
            orderRepository.MarkOrderAsDone(new DateTime(2020, 1, 1), _lastAddedOrder.OrderID);

            // Assert
            var actual = orderRepository.GetOrderByID(_lastAddedOrder.OrderID);
            expected.Should().BeEquivalentTo(actual, options => options.IncludingNestedObjects());
        }

        [Fact, TestPriority(8)]
        public void DeleteOrder_Success()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));

            // Act
            orderRepository.DeleteOrder(_lastAddedOrder);

            // Assert
            var deletedOrder = orderRepository.GetOrderByID(_lastAddedOrder.OrderID);
            deletedOrder.Should().BeNull();
        }

        [Fact]
        public void GetGoodItemsBought_Success()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));
            var expected = GoodItemsBought;

            // Act
            var actual = orderRepository.GetGoodItemsBought(TestCustomerID);

            // Assert
            expected.Should().BeEquivalentTo(actual, options => options.IncludingNestedObjects());
        }

        [Fact]
        public void GetExtendedOrderDetails_Success()
        {
            // Arrange
            DbProviderFactories.RegisterFactory(_providerInvariantName, _dbProviderFactory);
            var orderRepository = new OrderRepository(_connectionString, DbProviderFactories.GetFactory(_providerInvariantName));
            var expected = ExtendedOrderDetails;

            // Act
            var actual = orderRepository.GetExtendedOrderDetails(TestOrderID);

            // Assert
            expected.Should().BeEquivalentTo(actual, options => options.IncludingNestedObjects());
        }
    }
}   
